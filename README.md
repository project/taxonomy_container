## Taxonomy Container

## Summary
The Taxonomy Container module provides a select list with optgroups for taxonomy
terms selection.

## Installation
Install module as usual.

## Usage
Open settings page for a given taxonomy field and set reference method to
"Taxonomy term selection (with groups)".
